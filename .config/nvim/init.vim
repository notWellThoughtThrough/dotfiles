call plug#begin('~/.config/nvim//plugged')
  
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'flazz/vim-colorschemes'
Plug 'mhinz/vim-startify'
Plug 'derekwyatt/vim-scala'
call plug#end()
